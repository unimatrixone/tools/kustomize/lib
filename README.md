# Kustomize Library for Unimatrix Clusters


## Deploying a HTTP proxy server

The `components/drone-http-proxy` deploys a HTTP proxy server.

- Ensure that the cluster has nodes with the following taints for both
  `NoSchedule` and `NoExecute`:
  - `ops.unimatrixone.io/allow-egress-http`
- Ensure that the tainted nodes have external connectivity to ports `80`,
  `443`.
- Set an image for `http-proxy` in `kustomization.yml`:

  ```
  images:
  - name: http-proxy
    newName: eu.gcr.io/unimatrixone/squid
    newTag: latest
  ```
